import * as React from 'react';
import Routes from "./Routes";
import './static/css/App.css';

export default function App() {
  return (
    <Routes />
  );
}