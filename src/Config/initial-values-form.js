const initialValuesForm = {
    name: '',
    cpf: '',
    share_capital: '',
    zip_code: '',
    city: '',
    state: '',
    address: '',
    complement: ''
};

export { initialValuesForm };