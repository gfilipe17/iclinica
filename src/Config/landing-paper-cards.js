const listPaperCardItems = [
    {
        title: 'Gerencie sua clínica médica de qualquer lugar',
        info: 'Tratamos com muita seriedade o bem mais precioso de sua clínica médica: seu paciente. Seus dados estão seguros com o iClinic.',
        img: 'https://lirp.cdn-website.com/f9aa6a73/dms3rep/multi/opt/img1-1920w.jpg'
    },
    {
        title: 'Prezamos pela segurança da informação emédica',
        info: 'Conte com a mesma tecnologia de grandes empresas. Nosso software para clínicas é em nuvem, com a infraestrutura da Amazon (AWS).',
        img: 'https://lirp.cdn-website.com/f9aa6a73/dms3rep/multi/opt/img-pro1-1920w.png'
    },
    {
        title: 'Use a tecnologia para transformar vidas',
        info: 'Desde a hora de agendar até após a consulta, ofereça comodidade e garanta a tranquilidade no atendimento ao paciente.',
        img: 'https://lirp.cdn-website.com/f9aa6a73/dms3rep/multi/opt/img-pro2-1920w.png'
    }
];

export default listPaperCardItems;