const listPaperInfoItems = [
    "Clínicas Médicas",
    "Clínicos Gerais",
    "Oftalmologistas",
    "Cardiologistas",
    "Psiquiatras",
    "Dermatologistas",
    "Endocrinologistas",
    "Outras especialidades"
];

export default listPaperInfoItems;