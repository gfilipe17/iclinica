import axios from 'axios';

const BASE_URL = 'https://61959cf574c1bd00176c6df2.mockapi.io/api/v1/';

axios.defaults.baseURL = BASE_URL;

export { axios };

